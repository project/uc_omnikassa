Omnikassa PSP payment module for Ubercart

PREREQUISITES

- Drupal 6.X

INSTALLATION

Drupal:
Install and activate this module like every other Drupal
module.

Backend:
At /admin/store/settings/payment/edit/methods you can fill out the required parameters.
- Merchant ID
- Secret key 
- Key version (defaults to 1 and it usually stays that way)
- Reference (a string that preceeds the ordernumber)
- Available payment methods (iDEAL, CreditCard, PayPal, etc)
- Mode (test / production)
- Currency
- Customer language
- Debug callback (logs key points to watchdog)

  FEATURES

- Payment method choice after redirection to OmniKassa
- Handles (offline) status changes
- Select payment methods to show on the OmniKassa page
- All settings adjustable in admin form (see payment methods in your store configuration)


INFORMATION
Need an account?
Get it here:
http://www.rabobank.nl/bedrijven/producten/betalen_en_ontvangen/geld_ontvangen/rabo_omnikassa/
(in Dutch)

THANKS
Thanks to the helpful Ubercart team and forum users for tips and answers.

DEVELOPMENT
This module is developed, maintained and distributed bij Web-Beest. 
We can be contracted for Drupal/Ubercart projects or (payment) module building. Mail us: info {at} web-beest {dot} nl.

www.web-beest.nl
